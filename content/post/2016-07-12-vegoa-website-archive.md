+++
title = "Vegoa website archive"
date = "2016-07-12T17:31:43+02:00"
tags = ["website", "archive", "tools", "transparency", "vegoa"]
categories = ["transparency"]
menu = ""
banner = "banners/website.jpg"
authors = ["Geraldine Starke"]
+++

Several months ago, we created (Tiago (design), Caner (architecture) and I (content)), the first Vegoa official website to gather all the information around the project, that we had started to define as a group with the founders.

This website was removed few weeks ago now and replaced by a new one, without the approval of any member. During this process, changes have been made to that website (content changes). These modifications were made on the sole decision of few members without the consent of the others. These modifications are drastic changes that totally redefine Vegoa's principles and values. I will try to list them here under:

* **Economy section**: the economy section was totally removed, including the basic income part, although it is a main pillar in the Vegoa concept. Some members, and the creator, are trying now to pass to a gift economy, again against members will. These changes are only been done because the economy part of Vegoa was brought by Caner Candan, and these members don't want to have to need him anymore. Furthermore, **the basic income is part of the manifest** (point 9) and is therefore an important part of Vegoa as every member is supposed to receive a basic income (it is still visible on the new vegoa website as well). Here is a link to this economy page that was removed:

Economy page: https://vegoa.vegoans.org/en/economy/

* **FAQ**: The whole FAQ of the Vegoa homepage has just been deleted. A question from the Organisation's page FAQ concerning the use of the Vegetas (currency that was supposed to be used in Vegoa before they took the economy part away) has been removed. The FAQ of the manifest, explaining how the manifest should be updated, was removed as well. Here you can find them again:

FAQ homepage: https://vegoa.vegoans.org/en/

FAQ organisation: https://vegoa.vegoans.org/en/organisations/

FAQ manifest:
https://vegoa.vegoans.org/en/manifest/

* **Joining page**: the previous joining page was totally modified and most of the information was removed. For example, a very important information explained how the 500€ membership fee will be used, has been removed. Here you can find again this page at its original state:

Joining page:
https://vegoa.vegoans.org/en/joining/

* **Vegan Hills page**: minor modifications, but the total price of the Vegan Hills land (425k€) and it's capacity (30-40 families) has been removed.

* **Vegan Hills manifest**: Changes have been made to the Vegan Hills manifest, although it requires 90% vote from the Vegoans. The changes are the removal of the points 4 and 10 of the initial manifest as well as the modification of point 3. The manifest's FAQ has also been deleted. Here is the original Vegan Hills manifest, as well as the FAQ, before it was removed:

Vegan Hills manifest:
https://vegoa.vegoans.org/en/joining/vegan-hills/

* **Contact page**: The contact page was totally removed. You can find the archive here (we temporarily changed the email and fb page as their were leading to tools solely owned by the creator and are working on a solution that would make the contact info decentralized):

Contact page:
https://vegoa.vegoans.org/en/joining/contact/

* **Blog**: The Vegoa blog was entirely deleted as well. Here you can find it again:

Blogs:
https://vegoa.vegoans.org/en/blogs/

* **French website**: The french version of the website was totally removed, again without any consent. Please find the french version here:

French version:
https://vegoa.vegoans.org/fr/

* **Hostel organisation**: The hostel organisation page was added to the new website although this project has not been approved by the members and as this project implies selling a part of the land, it affects all the members.

* **Villages page**: A villages page has been added to the new website as well, although this project was not approved by members. Vegan Hills itself is a land organisation and it was never discussed that land organisation can be divided in multiple land organisation (see land organisation definition). Furthermore the rules indicated in this page to divide the land were not decided collectively.

New changes will probably come, and more will be removed. As there is no transparency, there is no way for members and people interested in the project, to track the website and the changes made to it. Decisions taken by one person, or a few people, are being made official without the consent of the others, and that is possible because of the ownership of the tools belonging to the creator (more will be posted about that soon).

That's why, as lots of people have noted these changes to the website, we decided to restore the old version of the website. Thanks to that, everyone can now compare the two websites by themselves and see those changes.

Archive of the initial website:
https://vegoa.vegoans.org/en/
