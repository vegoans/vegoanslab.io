+++
title = "The Vegoans Community Forum"
date = "2016-06-19T13:50:46+02:00"
tags = ["forum"]
categories = ["tools"]
menu = ""
banner = "banners/forum.jpg"
authors = ["Geraldine Starke"]
+++

A forum for the Vegoans Community has been created so there is an easy way for everyone that wishes to communicate about this project, get to know each other, exchange ideas and discuss about different subject.

The forum is public, everyone has access to it. Sadly there is no way (that we know) to have a decentralized forum. Meaning, the creators of the forum can close it. Let’s say this is a tool to help people communicate, but should not be used for official announcements or decisions (it should at least be duplicated in one of the decentralized tool available).

However, we will create a script that exports the content of the forum on a daily basis and hosts it on a git repository, ensuring this way that all the forum’s content is always accessible to anyone and can not be deleted. This way, if the creators decide to delete the forum, anybody can access the saved content on the git repository and clone it on a new forum. There will be multiple admins as well, and they have the possibility to export the content of the forum when they wish.

To access the forum please go to: https://community.vegoans.org
