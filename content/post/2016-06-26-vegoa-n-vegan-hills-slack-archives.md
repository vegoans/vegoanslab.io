+++
title = "Vegoa and Vegan-Hills Slack Archives"
date = "2016-06-26T16:50:46+02:00"
tags = ["slack", "archive", "vegoa", "vegan-hills"]
categories = ["tools", "archives", "transparency"]
menu = ""
banner = "banners/slack.jpg"
authors = ["Geraldine Starke"]
+++

Since several months we, at Vegoa, were using this great working tool *Slack*. It allowed us to discuss, make decision and answer everyone’s questions. It gave the opportunity to every new member to read the discussions, inform himself and eventually participate.

Last week, for several reasons that will not be explained here, these slack groups were deleted. However, after multiple members asked for, we decided to re-open them but only as archives. These groups are now available again for anybody to read and search for information but are not operative anymore.

Now thanks to the new tools that have been created, these decisions will not have to be taken anymore as the tools and their content are not deletable. They are also totally public and transparent. These tools can be used, like before in slack to discuss/debate/ask questions (forum) or to post articles and informative content (blogs).

We believe this change will prove our willing of full transparency and more importantly decentralized decision making. We hope that they will be largely used by members but also everyone wishing to participate to Vegoa’s community in a way.

Here are the link to the slack archives (they will also be added to the website):

* http://vegoa-slack-archive.vegoans.org/
* http://veganhills-slack-archive.vegoans.org/

Obviously and for privacy reasons only the public channels of discussions are made available in these archives.
