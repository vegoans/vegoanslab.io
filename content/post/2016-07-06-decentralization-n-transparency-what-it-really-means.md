+++
title = "Decentralization and Transparency, What It Really Means?"
date = "2016-07-06T20:50:46+02:00"
tags = ["decentralization", "transparency"]
categories = ["Concepts"]
menu = ""
banner = "banners/git.gif"
authors = ["Caner Candan"]
+++

Come on you never heard about the word decentralization and transparency yet? Nowadays, they are present very often in public ads from politicians to big corporates through non-profit organizations, words such as centralization, decentralization, obfuscation, transparency. Edward Snowden, Aaron Swartz, Richard Stallman or Lawrence Lessig are all used to advocate about the power of decentralization that can protect our freedoms. But what does it really means? Lot of organizations are using them but are they really consistent with the real meaning of these words. And what about Vegoa?

My perception is the following:

* A material that is owned by a single entity is not decentralization.
* A material that doesn't say all about the inner truth is not transparency.
* A material that is maintained by a single infrastructure is not decentralization.
* A material that cannot be tracked from its origin is not transparency.

**So what are they actually?** Decentralization means that anyone can get a copy of the whole material including the inherent data structure and history of updates that apply to it over its lifecycle. Basically it means I can replay the whole story of this material thanks to my copy of the material without being dependent in anyway to the original material or owner of the material. I should also be protected by legal terms that allow me to reuse the material without having to ask permissions to the original material owner. Therefore a material is something that can be copied not stolen.

Transparency is the causal effect of decentralization in a way, it enables to get access over the whole story of a decentralized material at any moment of its existence. One can therefore compare two states in different times of the same material and see what changes occurred during this time range.

## Decentralized and Transparent Vegoa

Back to Vegoa, what does this all means then? Currently, we are already using several tools at Vegoa, but unfortunately none of them are consistent with the early definition I gave. As an example, these are a few materials and tools that are used at Vegoa and so far reported violations linked to them:

* [Vegoa domain name](https://gitlab.com/vegoa/violations/issues/14)
* [Vegoa website](https://gitlab.com/vegoa/violations/issues/16)
* [Vegoa public communication channels (facebook and twitter)](https://gitlab.com/vegoa/violations/issues/17)
* [Vegoa private communication channel (slack teams)](https://gitlab.com/vegoa/violations/issues/8)
* [Vegoa bank account](https://gitlab.com/vegoa/violations/issues/18)
* [Vegoa email server (google apps)](https://gitlab.com/vegoa/violations/issues/15)
* [Vegoa and Vegan-hills manifest agreements](https://gitlab.com/vegoa/violations/issues/1)
* Vegoa artwork materials (logos, icons)
* Vegan-hills resources exploitation

Basically most of the violations are related with the single entity ownership and centralized one-sided decision creating issues that we will attempt to solve.

In my next posts, I will show an efficient and practicable way to solve each of these issues. We are going to talk about the Open-Source workflow, distributed control version system and repositories. But we are also going to talk about an efficient way to contribute to the project with multiple contributors and save time based on asynchronous communication methods.
