+++
title = "How Can I Write a Post on the Vegoans Community?"
date = "2016-07-09T18:26:00+02:00"
tags = ["git", "blog", "post"]
categories = ["tools"]
menu = ""
banner = "banners/blogging.jpg"
authors = ["Caner Candan"]
+++

In [my previous post](/decentralization-and-transparency-what-it-really-means/) I introduced the use of decentralized and transparent tools such as Git and its powerful way to keep a print on every updates that happens on a material. Although we knew that Git was commonly use for managing software projects source code, we wanted to extend its original usage to other materials. In this post I will tell you how you can use this new blogging environment based on Git in order to write new article or just suggest improvements on existing contents. By using Git for the Vegoans Community blogging environment we allow anyone to be able to contribute on the blog content even those that are not actual members of the Vegoans Community.

Here is a short clip that shows how to create a blog post through Gitlab.

{{< youtube fKONRepOdwU>}}

# The new blogging environment

Here we go. Git is a distributed version control system means your project is actually stored in a repository of files and anytime you commit new modifications, the Git repository records a print of the update so that anybody that contribute to the project can actually go backward in the repository history and see all the updates that applied from the whole contributors and those from the very beginning of the project. Although the Git repository can be duplicated and make the materials data decentralized, we still require a Git hosting server to expose the repository online, so people can fetch it and contribute. Several services exist such as Github, Bitbucket, Gitlab, Gogs or just setting up an adhoc Git server. In the Vegoans Community, we made the choice to use Gitlab, first because compared with most of its competitors it's entirely Open Sourced. At anytime you can install your own instance of Gitlab on your own server and transfer the whole content. And second, because it offers very powerful features such as Merge Requests Approving flow. I am going to detail this later. Therefore there is a *group* on Gitlab called [**vegoans**](https://gitlab.com/groups/vegoans) that stores all the Git repository based projects including the current blog of the Vegoans Community.

# Signing up on Gitlab.com

First you will be required to create an account on [Gitlab](https://gitlab.com). To do so, you can directly click on [this link](https://gitlab.com/users/sign_in) and fill the *Create an account* form and click on the *Sign up* green button.

![Create an account on Gitlab.com](/images/gitlab-signup.png)

If you already have a Google, Twitter, Github or Bitbucket account, you can actually connect to Gitlab without having to create a new account.

![Sign in with](/images/gitlab-signin-with.png)

Once your account is created, you can sign in anytime just by filling the *Sign in* form with your Gitlab username and password.

![Sign in on Gitlab](/images/gitlab-signin.png)

# Blog Project

Once you are successfully signed in, you should see the *Gitlab dashboard* with no existing projects yet.

![Gitlab Dashboard](/images/gitlab-dashboard.png)

Now you are ready to get access to the public [Vegoans Gitlab group](https://gitlab.com/groups/vegoans). Most of the Vegoans Community, as long as there is no obvious reason to keep repositories private, are public, so anybody online can see the source code of the projects and even contribute on them. If you are willing to become a member of the group and contribute actively on the projects, you will need to request your will to become a member of the group by clicking on the *Request Access* button. All the members will therefore get notified by email and they should approve your membership very quickly.

![Gitlab Request Access](/images/gitlab-request-access.png)

Being a member of the group gives you a few more privileges. You will be able to access to the private repositories of the group such as the content of the Vegoans Community Domains certification we keep unlisted. Now, from the Vegoans Community Group page, you should see the public project **vegoans.gitlab.io**.

![Blog project](/images/gitlab-blog-project.png)

You can click on the project and get access to the Blog project board.

![Blog project board](/images/gitlab-blog-project-frontpage.png)

The Gitlab project board allows to access to all the information related with the project repository and additionally provides tools such as an *Issues Tracker* and some others that I will eventually explain in my future posts. For now, we only need to know how we can create a post on the blog. Therefore you need to access to the files hierarchy of the project repository by going on the [**Repository** section](https://gitlab.com/vegoans/vegoans.gitlab.io/tree/master). You should see the following files hierarchy appearing on your screen.

![Blog file hierarchy](/images/gitlab-blog-files-hierarchy.png)

Something important to know is that all the posts of the blog are actually stored as a textual file within the folder named **_posts**. So before create a new post, you will need to click on the **_posts** folder. Lost? Here is [a link](https://gitlab.com/vegoans/vegoans.gitlab.io/tree/master/_posts) to access directly to the **_posts** folder.

![Gitlab Blog Posts Folder](/images/gitlab-blog-posts-folder.png)

From the **_posts** folder, you can click on the **plus** button available on the top (see the following screenshot) and select the option **New file** in the dropdown menu in order to create a new post.

![Gitlab new file menu](/images/gitlab-new-file-menu.png)

You will therefore be directed to a new page in order to create your new file.

![Gitlab new file](/images/gitlab-new-file.png)

# Git Concepts

Interesting things start now. First some concepts need to be explained here. A Git repository can have several branches that enable to have different versions of the project repository. In software development, Git branches are commonly used to separate the development version of the product from the production one. What is interesting to remember so far is that the **master** branch is actually the default and the main branch of any Git repository. Another interesting feature of branches is that you can actually merge two branches, one into another. This is very useful, for instance if you want to improve something in your own branch without polluting the **master** branch and only merge to the **master** once you finished.

In the Vegoans Community, we are protecting all the Git repositories **master** branch so nobody even our members are not allowed to merge their branches to the Vegoans Community group projects **master** branches. But that's not all, they cannot also directly make changes on the files available in the **master** branch thanks to a *protected branch* feature.

So how can someone contribute to the repository and suggest an update, create a new blog post? The short answer is by using **Fork** and **Merge Request**. Since you are not allowed to make directly changes to the Blog project, once you created a new file, Gitlab automatically *Forked* (duplicated) the project and put the duplicated project into your user space with you own branch. And once you will commit your changes (at this stage by creating a new file) to your version of the project, you will be then invited to create a merge request, in order to request a merge of your version of the repository with the original Blog project repository.

# Merge Request

Sending a merge request with your new post doesn't mean that your post will be automatically merged within the original Blog project. Indeed, it requires actually a minimum of approvals from the group members in order to be approved. The *Merge Requests* section of the project allowed to follow up the status of the request. You will also know if some improvements are required before approving it or if it does not respect the [Contributing Guide](/CONTRIBUTING.md).

# Create a new post

Fair enough, we are ready to create our new blog post now. The Blog posts use the [Markdown styling syntax](https://gitlab.com/help/markdown/markdown) to decorate the content in order to add headlines, bold and italic texts, links, images and tables into your post. If you are not familiar with the Markdown styling syntax, you can take a look to [this guide](https://gitlab.com/help/markdown/markdown) to have a complete documentation about the possibility offered by Markdown though I will list some syntaxes below.

First, since your post will be stored in the project as a file, you will be required to name your post file. Here are some examples of file names we already used for our past posts:

* *2016-05-16*-**an-agile-cto-checklist**.*md*
* *2016-05-31*-**behavior-driven-development-saas**.*md*
* *2016-07-06*-**the-power-of-git**.*md*

Basically, the file name should only contains alphanumerical characters without accents and whitespaces and use instead dashes to separate words. The file name is prefix with the current date in the ISO format (e.g. 2016-07-09) followed by a *dash* and the post title you want to use as a title for your new post, and suffixed with *.md* file extension (Markdown file extension).

![New filename](/images/gitlab-new-file-filename.png)

Before starting to write your blog post, a last step is needed. You need to add the following header variables in the top of your post content.

```
+++
title = "How Can I Write a Post?"
date = "2016-07-06T00:26:00+02:00"
tags = ["git", "blog", "post"]
categories = ["tools"]
menu = ""
banner = "banners/blogging.jpg"
authors = ["Caner Candan"]
+++
```

You can update the header variables according to your post and following these rules:

* the post header must have the `+++` line before and after.
* the variable `title` is the actual title of your blog post.
* the variable `date` is the publication date of your post following the ISO format `YYYY-MM-DDThh:mm:ss+timezone`.
* the variable `tags` is a list of tags related with your post. You have to use the `[` and `]` before and after the list and separate each tag by a comma and use double-quote around each single tag.
* the variable menu cane remain empty for now, I will introduce in another post the Static Content Generator used by the blog and explain these related features more in details.
* the variable `banner` is the path to the post banner image. I will show you later how to upload the image to the repository.
* the variable `authors` is a list of authors that actually wrote the post. You have to use the `[` and `]` before and after the list and separate each author name by a comma and use double-quote around each single author name.

Now you are ready to write your post content. You can find below some examples of Markdown syntax usage in order to be familiar with it.

* To bold **your text**, you need to use the mark `**` around your text as follows: `**some words**`.
* To write in italic *your text*, you need to use the mark `*` around your text as follows: `*some words*`.
* To add a link to [your text](#), you need to use the following syntax: `[some words](https://www.awesome-website.xyz)`. You place between `[` and `]` the text of the link and between `(` and `)`, the actual URL of the link.
* To add a buddeted list of items, you just need to use the mark `*` at the beginning of the line followed by a whitespace as follows: `* item 1`.
* To add an image, use the following syntax: `![some words to describe the image](https://www.awesome-website.xyz/images/my-awesome-image.png)`
* To add a headline, you just need to use the mark `#` or `##` or `###` according to your headline size followed by the actual headline title. `#` is the biggest size. For instance: `## My title`.
* To add a code sample in your post, you have two ways to do it, either inlined or in a block. For inlined code, you can use the mark **\`** around your code sample which may give as a result: `My code example`. For block of code, you can use the mark **\`\`\`** around the code block as follows:

```
<html>
  <body></body>
</html>
```

Go ahead write you post content and when you have finished you can keep reading the rest of the explanation.

![Write your post](/images/gitlab-write-post.png)

When you finished you can click on the green **Commit Changes** button, on the bottom of the page. You should be directed to a new page where you are invited to sent a *Merge Request*.

![New Merge Request](/images/gitlab-new-merge-request.png)

You can edit the Merge Request title and add a description to describe the reason you want the members to merge your new post and click on the green **Submit merge request** button.

![New Merge Request created](/images/gitlab-merge-request-created.png)

You are done, your new post merge request has been sent, now the Vegoans Community members will need to approve it in order to merge your post into the Vegoans Community Blog project.
