You can help to improve the [vegoans.org](https://vegoans.org/) website by sending merge requests to this repository.
Thank you for your interest and help!

We don't mind people inside and outside the company making suggestions to change significant things.
Feel free to make a proposal, we can discuss anything and if we don't agree we'll feel free not to merge it and thank you for caring about it.

By submitting code you agree to the [license](LICENSE).

For more information about the technicalities of contributing please see the [README](README.md).
